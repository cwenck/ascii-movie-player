#!/usr/bin/env python

import signal
import curses
from time import sleep
import os

total_frames = 3200
current_frame = 0
frame_lines = 14

DEBUG = False

def signal_handler(signal, frame):
    pass

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTSTP, signal_handler)

def frame_text(frame):
    starwars_file = open(os.path.dirname(os.path.realpath(__file__)) + '/ascii_starwars.txt', 'r')
    # first line of each frame is the delay
    start_line = (frame * frame_lines) + 1
    end_line = start_line + frame_lines - 1
    frame_string = []
    delay = 1
    for i, line in enumerate(starwars_file):
        
        if i == start_line - 1:
            delay = int(line.replace('\n', ''))
        

        if i <= end_line and i >= start_line:
            frame_string.append(line)

    return delay, frame_string

def get_frame():
    global current_frame
    if current_frame > total_frames:
        return None
    frame_data = frame_text(current_frame)
    current_frame += 1
    return frame_data

# Size Global Vars
start_x = 0
start_y = 0
max_x = 0
max_y = 0
    
def main(win):
    #global start_x, start_y, max_x, max_y
    
    def quit_sequence_pressed():
        key_seq = ['s', 'w', 't', 'a', 'q']

        for char in key_seq:
            key = None
            try:
                key = win.getkey()
            except:
                return False
            if key != char:
                return False
        return True


    win = curses.initscr()
    curses.noecho() # Don't print keys
    curses.cbreak() # Don't need to press enter to register keys
    curses.halfdelay(2)
    curses.curs_set(0)
    win.keypad(True)
    win.scrollok(False)

    def calc_size():
        global start_x, start_y, max_x, max_y
        start_y = (max_y / 2) - 7   
        start_x = (max_x / 2) - 34
        max_y, max_x = win.getmaxyx()
    
    calc_size()
    # Starwars is 67x by 13y

    frame = []
    while frame is not None and not quit_sequence_pressed():
        
        if curses.is_term_resized(max_y, max_y):
            calc_size()
        
        win.clear()
        delay, frame = get_frame()
        if DEBUG:
            try:
                win.addstr(5, 5, 'Frame: ' + str(current_frame))
                win.addstr(1, 5, 'Start_x: ' + str(start_x))
                win.addstr(2, 5, 'Start_y: ' + str(start_y))
                win.addstr(3, 5, 'Max_x: ' + str(max_x))
                win.addstr(4, 5, 'Max_y: ' + str(max_y))
                win.vrefresh()
            except:
                pass

        #win.addstr(10, 10, str(frame))
        for y in range(start_y, start_y + 13):
            try:
                win.addstr(y, start_x, frame[y - start_y].replace('\r\n', ''))
            except:
                pass
        win.refresh()
        # 15 fps
        #sleep(1.0/15.0)
        sleep(1.0/15.0 * delay)
curses.wrapper(main)
